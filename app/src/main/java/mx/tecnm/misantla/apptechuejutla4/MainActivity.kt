package mx.tecnm.misantla.apptechuejutla4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import mx.tecnm.misantla.apptechuejutla4.databinding.ActivityMainBinding
import mx.tecnm.misantla.apptechuejutla4.fragments.OpcionDosFragment
import mx.tecnm.misantla.apptechuejutla4.fragments.OpcionUnoFragment

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding
    lateinit var fragment : Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnOpcion1.setOnClickListener {
         fragment = OpcionUnoFragment()
            insertarFragmento()
        }

       binding.btnOpcion2.setOnClickListener {

           fragment = OpcionDosFragment()
           insertarFragmento()
       }

    }

    private fun insertarFragmento() {
        fragment?.let {
            supportFragmentManager.beginTransaction().replace(R.id.contenedor,it).commit()
        }
    }
}